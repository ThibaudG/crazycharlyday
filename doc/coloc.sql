﻿
/*!40101 SET NAMES utf8 */;

CREATE OR REPLACE TABLE `logement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `places` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;


INSERT INTO `logement` (`id`, `places`) VALUES
(1, 3),
(2, 3),
(3, 4),
(4, 4),
(5, 4),
(6, 4),
(7, 4),
(8, 5),
(9, 5),
(10, 6),
(11, 6),
(12, 6),
(13, 7),
(14, 7),
(15, 8),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 3);


CREATE OR REPLACE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `age` int(2) NOT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;


INSERT INTO `user` (`id`, `nom`, `message`, `age`, `image`) VALUES
(1, 'Jeanne', 'aime la musique ♫', 18, '1.jpg'),
(2, 'Paul', 'aime cuisiner ♨ ♪', 56, '2.jpg'),
(3, 'Myriam', 'mange Halal ☪', 85, '3.jpg'),
(4, 'Nicolas', 'ouvert à tous ⛄', 21, '4.jpg'),
(5, 'Sophie', 'aime sortir ♛', 22, '5.jpg'),
(6, 'Karim', 'aime le soleil ☀', 65, '6.jpg'),
(7, 'Julie', 'apprécie le calme ☕', 102, '7.jpg'),
(8, 'Etienne', 'accepte jeunes et vieux ☯', 25, '8.jpg'),
(9, 'Max', 'féru de musique moderne ☮', 69, '9.jpg'),
(10, 'Sabrina', 'aime les repas en commun ⛵☻', 23, '10.jpg'),
(11, 'Nathalie', 'bricoleuse ⛽', 97, '11.jpg'),
(12, 'Martin', 'sportif ☘ ⚽ ⚾ ⛳', 88, '12.jpg'),
(13, 'Manon', '', 56, '13.jpg'),
(14, 'Thomas', '', 21, '14.jpg'),
(15, 'Léa', '', 20, '15.jpg'),
(16, 'Alexandre', '', 23, '6.jpg'),
(17, 'Camille', '', 65, '17.jpg'),
(18, 'Quentin', '', 19, '18.jpg'),
(19, 'Marie', '', 20, '19.jpg'),
(20, 'Antoine', '', 21, '20.jpg'),
(21, 'Laura', '', 87, '21.jpg'),
(22, 'Julien', '', 25, '22.jpg'),
(23, 'Pauline', '', 19, '23.jpg'),
(24, 'Lucas', '', 84, '24.jpg'),
(25, 'Sarah', '', 74, '25.jpg'),
(26, 'Romain', '', 21, '26.jpg'),
(27, 'Mathilde', '', 53, '27.jpg'),
(28, 'Florian', '', 26, '28.jpg');

CREATE OR REPLACE TABLE `groupe` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`createur` int(11) NOT NULL,
	`logement` int(11) NOT NULL,
	FOREIGN KEY (`createur`) REFERENCES user(`id`),
	FOREIGN KEY (`logement`) REFERENCES logement(`id`),
	PRIMARY KEY (`id`)
)   ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

INSERT INTO `groupe` (`id`, `createur`, `logement`) VALUES
(1, 3, 4);


CREATE OR REPLACE TABLE `gestionnaire` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`nom` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	PRIMARY KEY(`id`)
)   ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


INSERT INTO `gestionnaire` (`id`, `nom`) VALUES
(1, 'Pierre');


CREATE OR REPLACE TABLE `invitations` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`id_createur` int(11) NOT NULL,
	`id_invite` int(11) NOT NULL,
	`valide` boolean,
	FOREIGN KEY (`id_createur`) REFERENCES groupe(`createur`),
	FOREIGN KEY (`id_invite`) REFERENCES user(`id`),
	PRIMARY KEY(`id`)
)   ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO `invitations` (`id`, `id_createur`, `id_invite`) VALUES
(1, 3, 5),
(2, 3, 12),
(3, 3, 15);