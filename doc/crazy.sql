-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 09 Février 2017 à 14:54
-- Version du serveur :  10.1.16-MariaDB
-- Version de PHP :  5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `crazy`
--

-- --------------------------------------------------------

--
-- Structure de la table `gestionnaire`
--

CREATE TABLE `gestionnaire` (
  `id` int(11) NOT NULL,
  `nom` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `gestionnaire`
--

INSERT INTO `gestionnaire` (`id`, `nom`) VALUES
(1, 'Pierre');

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

CREATE TABLE `groupe` (
  `id` int(11) NOT NULL,
  `createur` int(11) NOT NULL,
  `logement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `groupe`
--

INSERT INTO `groupe` (`id`, `createur`, `logement`) VALUES
(1, 3, 4);

-- --------------------------------------------------------

--
-- Structure de la table `invitations`
--

CREATE TABLE `invitations` (
  `id` int(11) NOT NULL,
  `id_createur` int(11) NOT NULL,
  `id_invite` int(11) NOT NULL,
  `valide` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `invitations`
--

INSERT INTO `invitations` (`id`, `id_createur`, `id_invite`, `valide`) VALUES
(1, 3, 5, NULL),
(2, 3, 12, NULL),
(3, 3, 15, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `logement`
--

CREATE TABLE `logement` (
  `id` int(11) NOT NULL,
  `places` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `logement`
--

INSERT INTO `logement` (`id`, `places`) VALUES
(1, 3),
(2, 3),
(3, 4),
(4, 4),
(5, 4),
(6, 4),
(7, 4),
(8, 5),
(9, 5),
(10, 6),
(11, 6),
(12, 6),
(13, 7),
(14, 7),
(15, 8),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 3);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nom` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `age` int(2) NOT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `message`, `age`, `image`) VALUES
(1, 'Jeanne', 'aime la musique ♫', 18, '1.jpg'),
(2, 'Paul', 'aime cuisiner ♨ ♪', 56, '2.jpg'),
(3, 'Myriam', 'mange Halal ☪', 85, '3.jpg'),
(4, 'Nicolas', 'ouvert à tous ⛄', 21, '4.jpg'),
(5, 'Sophie', 'aime sortir ♛', 22, '5.jpg'),
(6, 'Karim', 'aime le soleil ☀', 65, '6.jpg'),
(7, 'Julie', 'apprécie le calme ☕', 102, '7.jpg'),
(8, 'Etienne', 'accepte jeunes et vieux ☯', 25, '8.jpg'),
(9, 'Max', 'féru de musique moderne ☮', 69, '9.jpg'),
(10, 'Sabrina', 'aime les repas en commun ⛵☻', 23, '10.jpg'),
(11, 'Nathalie', 'bricoleuse ⛽', 97, '11.jpg'),
(12, 'Martin', 'sportif ☘ ⚽ ⚾ ⛳', 88, '12.jpg'),
(13, 'Manon', '', 56, '13.jpg'),
(14, 'Thomas', '', 21, '14.jpg'),
(15, 'Léa', '', 20, '15.jpg'),
(16, 'Alexandre', '', 23, '6.jpg'),
(17, 'Camille', '', 65, '17.jpg'),
(18, 'Quentin', '', 19, '18.jpg'),
(19, 'Marie', '', 20, '19.jpg'),
(20, 'Antoine', '', 21, '20.jpg'),
(21, 'Laura', '', 87, '21.jpg'),
(22, 'Julien', '', 25, '22.jpg'),
(23, 'Pauline', '', 19, '23.jpg'),
(24, 'Lucas', '', 84, '24.jpg'),
(25, 'Sarah', '', 74, '25.jpg'),
(26, 'Romain', '', 21, '26.jpg'),
(27, 'Mathilde', '', 53, '27.jpg'),
(28, 'Florian', '', 26, '28.jpg');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `gestionnaire`
--
ALTER TABLE `gestionnaire`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `createur` (`createur`),
  ADD KEY `logement` (`logement`);

--
-- Index pour la table `invitations`
--
ALTER TABLE `invitations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_createur` (`id_createur`),
  ADD KEY `id_invite` (`id_invite`);

--
-- Index pour la table `logement`
--
ALTER TABLE `logement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `gestionnaire`
--
ALTER TABLE `gestionnaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `groupe`
--
ALTER TABLE `groupe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `invitations`
--
ALTER TABLE `invitations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `logement`
--
ALTER TABLE `logement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD CONSTRAINT `groupe_ibfk_1` FOREIGN KEY (`createur`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `groupe_ibfk_2` FOREIGN KEY (`logement`) REFERENCES `logement` (`id`);

--
-- Contraintes pour la table `invitations`
--
ALTER TABLE `invitations`
  ADD CONSTRAINT `invitations_ibfk_1` FOREIGN KEY (`id_createur`) REFERENCES `groupe` (`createur`),
  ADD CONSTRAINT `invitations_ibfk_2` FOREIGN KEY (`id_invite`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
