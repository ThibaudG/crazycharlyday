<?php

namespace ccd\vue;

use ccd\modele\User;
use util\HTML;
use util\HTML2;

class VueUser
{
    const AFF_USER = 1;
    const AFF_LISTE_USER = 2;

    public $objet;

    public function __construct($array)
    {
        $this->objet = $array;
    }

    public function render($id)
    {
       // $res = User::where('id', '=', $id)->first();
        $res = $this->affichUser($id);
        echo $res;
        //echo $res->message;
    }

    /**
     * M�thode permettant d'obtenir l'id d'un utilisateur pour l'affichage de l'utilisateur
     * @return code html
     */
    public function affichUser($id)
    {
		$html = HTML2::head();
		$html.= HTML2::header();
		$html.='<!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">';
        $html .= "<h2>Utilisateur : </h2>";
        $c_id = 0;
        $image="";
        $nom="";
        $message="";
        $u = User::all();
        foreach ($u as $key => $value) {
            if ($id == $value->id) {
                if($value->message==""){
                    $c_id = $value->id;
                    $nom = $value->nom;
                    $message="Aucune informations";
                    $image = $value->image;
                }else {
                    $c_id = $value->id;
                    $nom = $value->nom;
                    $message = $value->message;
                    $image = $value->image;
                }
            }
        }
        $html .= "Id : $c_id <BR> Nom : $nom <BR> Message : $message <BR> Image : <img src=\"../../web/img/user/$c_id.jpg\"> <br>";
		$html.=" </div>
            </div>
        </div>";
		$html.="</header>'".HTML2::foot();
		$html.=HTML2::footer();

        return $html;
    }


    public function listeUser()
    {
		$html = HTML::head();
		$html.= HTML::header();
		$html.='<!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">';
        $html .= "<h2>Liste des utilisateur</h2>
			<div>";

        $u = User::all();
		
        foreach ($u as $key => $value) {
            $nom = $value->nom;
            $photo = $value->image;
			$html .= "<form method=\"get\" action=\"user/$value->id\">";
            $html .= "<button type=\"submit\" href=\"user/$value->id\" style=\"text-decoration:none\" >$nom</button><br><br>";
			$html.="</form>";
        }
		
		// "$nom <button type=\"submit\" href=\"user/$value->id\" style=\"text-decoration:none\">Lien</button><br>";
		
		
		
		$html.=" </div></div>
            </div>
        </div>";
		$html.="</header>'".HTML::foot();
		$html.=HTML::footer();
        return $html;
    }
}