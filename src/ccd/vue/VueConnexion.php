<?php
namespace ccd\vue;
use util\HTML;
class VueConnexion
{
    public $objet;

    public function __construct($array){
        $this->objet = $array;
    }

    public function afficher(){
        $r = HTML::head();
		$r.= HTML::header();
		$r.='<!-- Header -->
    <header>';
        
		
		$r.='<div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Identifiez-vous</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section">
                <div class="container">
                    <div class="row"><div class="col-md-7">
                        <form class="form-horizontal" role="form" method="post" action="signin">
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputEmail3" class="control-label">Nom</label>
                                </div>
                                <div class="col-sm-10"><input type="text"  name="Utilisateur" class="form-control" id="inputEmail3" placeholder="Nom">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputPassword3" class="control-label">Password</label>
                                </div>
                                <div class="col-sm-10"><input type="password" name="mdp" class="form-control" id="inputPassword3" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Se souvenir de moi 
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default">Connexion</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-5"><p class="lead">Notre objectif: Permettre à chaque entreprise de répondre à des appels d\'offres en toute simplicitée.</p>
                    </div>
                </div>
            </div>
        </div>';
		$r.="</header>'".HTML::foot();
		$r.=HTML::footer();
        return $r;
    }


}