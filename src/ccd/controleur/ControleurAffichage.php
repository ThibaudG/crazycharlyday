<?php

namespace ccd\controleur;

use ccd\vue\VueIndex;
use ccd\vue\VueUser;
use ccd\vue\VueListeLogement;
use ccd\vue\VueDetailLogement;
use ccd\vue\VueInscription;
use ccd\vue\VueConnexion;

class ControleurAffichage
{
    public function __construct($http)
    {
        $this->httpRequest = $http;
    }

    public function afficherAccueil()
    {
        $v = new VueIndex($this->httpRequest);
        echo $v->afficher();
    }

    public function afficherListeLogement()
    {
        $v = new VueListeLogement($this->httpRequest);
        echo $v->afficher();
    }
    public function afficherDetailLogement($id)
    {
        $v = new VueDetailLogement($this->httpRequest);
        echo $v->afficher($id);
    }

    public function afficherUser($id)
    {
        $vue = new VueUser($this->httpRequest);
        echo $vue->affichUser($id);
    }

    public function listeProfil()
    {
        $vue = new VueUser($this->httpRequest);
        echo $vue->listeUser();
    }

    public function afficherConnexion()
    {
        $vue = new VueConnexion($this->httpRequest);
        echo $vue->afficher();
    }

    public function afficherInscription()
    {
        $vue = new VueInscription($this->httpRequest);
        echo $vue->afficher($this->httpRequest);
    }


}