<?php

namespace ccd\controleur;

    use ccd\modele\Groupe;
    use ccd\modele\User;
    use ccd\vue\VueIndex;
    use ccd\vue\VueUser;
    use ccd\vue\VueGroupe;

class ControleurGroupe
{
    public function __construct($http)
    {
        $this->httpRequest = $http;
    }

    public function creerGroupe($texte)
    {
        $v = new VueGroupe($this->httpRequest);
        echo $v->creer($texte);
        return new Groupe();
    }

    public function ajoutGroupe($id)
    {
        $v = new VueGroupe($this->httpRequest);
        echo $v->ajouter($id);
    }
	
	public function afficherGroupe($id)
	{
		$v = new VueGroupe($this->httpRequest);
		echo $v->afficher($id);
	}
	
    
}