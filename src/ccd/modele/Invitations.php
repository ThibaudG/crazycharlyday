<?php

namespace ccd\modele;

class Invitations extends \Illuminate\Database\Eloquent\Model {
	
    protected $table = "invitations";
    protected $primaryKey = "id";
    public $timestamps = false;	
}