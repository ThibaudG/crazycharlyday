<?php

namespace ccd\modele;

class Gestionnaire  extends \Illuminate\Database\Eloquent\Model {
	
    protected $table = "gestionnaire";
    protected $primaryKey = "id";
    public $timestamps = false;	
}