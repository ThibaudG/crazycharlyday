<?php

namespace ccd\modele;

class Groupe  extends \Illuminate\Database\Eloquent\Model {
	
    protected $table = "groupe";
    protected $primaryKey = "id";
    public $timestamps = false;
	
	
	function user()
    {
        return $this->hasMany('ccd\modele\Invitation','groupe_id');
    }
}