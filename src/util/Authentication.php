<?php
namespace util;

use ccd\modele\User;

class Authentication{
	public static function createUser($nom, $message, $age, $passWord1, $passWord2){
		if ($nom==""||$passWord1==""||$passWord2=="") {
			return false;
		}
		if ($passWord1!=$passWord2) {
			return false;
		}
		$g=User::where('nom','=',$nom)->first();
		if (isset($g)){
			return false;
		}
		else{
			if (strlen($passWord1)<3) {
				return false;
			}
			else{
				$g=new User();
				$g->nom=$nom;
				if ($message!="") {
					$g->message=$message;	
				}
				$g->age=$age;
				$g->password=password_hash($passWord1, PASSWORD_DEFAULT,['cost'=>10]);
				$g->save();
				return true;
			}
		}
	}
	public static function authenticate($nom, $passWord){
		$u=User::where('nom','=',$nom)->first();
		if (isset($u)) {
			if (password_verify($passWord,$u->password)) {
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}
	
	public static function loadProfile($nom){
		$u=User::where('nom','=',$nom)->first();
		$array = array('id'=>$u->id,
					   'nom'=>$u->nom);
		unset($_SESSION['profil']);
		$_SESSION['profil']=$array;
	}

	public static function disconnect(){
		if (isset($_SESSION['profil'])) {
			unset($_SESSION['profil']);	
		}
	}
}