<?php
session_start();

$directionDepuisRepertoire = "";
if(isset($repertoire)){
    $nbSlash = substr_count($repertoire, "/");
    for($i=1; $i<=$nbSlash; $i++){
        $directionDepuisRepertoire .= "../";
    }
}

if(!isset($_SESSION['idUtilisateur']) && $title != "Login"){
    header("Location: ".$directionDepuisRepertoire."login.php");
}

$listeReservation = "Mes réservations";
if(isset($_SESSION['type'])){
    if($_SESSION['type'] == "Admin"){
        $listeReservation = "Pannel administateur";
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<?php echo $directionDepuisRepertoire;?>bootstrap-datetimepicker-master/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $directionDepuisRepertoire;?>bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $directionDepuisRepertoire;?>style.css" />
    <title><?php echo $title;?></title>
</head>
<body>
<div class="titre">
    <section>
        <h1>Réservation de véhicules</h1>
        <a href="<?php echo $directionDepuisRepertoire;?>listeReservation.php"><?php echo $listeReservation;?></a>
    </section>
</div>
<nav>
    <section>
        <a href="<?php echo $directionDepuisRepertoire;?>listeVehicules.php">Véhicules</a>
        <a href="<?php echo $directionDepuisRepertoire;?>planning.php">Planning</a>
        <a href="<?php echo $directionDepuisRepertoire;?>disponibilite.php">Disponibilités</a>
        <a href="<?php echo $directionDepuisRepertoire;?>effectuerReservation.php">Nouvelle réservation</a>
    </section>
</nav>

<script>
/*    var nav = document.getElementsByTagName('nav')[0].getElementsByTagName('span');
    for(var i = 0;i<nav.length;i++) {
        nav[i].onclick = function () {
            var direction = getDirection(this.innerHTML);
            if(direction != -1) {
                document.location.href = direction;
            }
        }
    }

    function getDirection(texte) {
        var direction;
        switch (texte) {
            case "Véhicules":
                direction = "listeVehicules.php";
                break;
            case "Planning":
                direction = "planning.php";
                break;
            case "Mes réservations":
                direction = "listeReservation.php";
                break;
            case "Disponibilités":
                direction = "disponibilite.php";
                break;
            default:
                alert("Impossible de rédiriger : localisation inconnue");
                direction = -1;
        }
        return direction;
    }

    function changerSelection(texte){
        for(var i = 0;i<nav.length;i++) {
            if(nav[i].innerHTML == texte){
                nav[i].style.backgroundColor = '#cadaee';
            }else{
                nav[i].style.backgroundColor = 'white';
            }
        }
    }

    changerSelection("TITRE");*/

    var h = document.getElementById('someDiv').style.height;



</script>
<section class="centrale">