<?php 
session_start();
include_once 'vendor/autoload.php';
use Illuminate\Database\Capsule\Manager;
use ccd\controleur\ControleurAffichage;
use ccd\controleur\ControlerLogin;
use ccd\controleur\ControleurAuthentication;
use ccd\controleur\ControleurGroupe;


use conf\Eloquent;

\Slim\Slim::registerAutoloader();

Eloquent::init('src/conf/config.ini');

$app = new \Slim\Slim();

$app->get('/',function() use ($app){
    $control = new ControleurAffichage($app);
    $control->afficherAccueil();
})->name('accueil');

$app->get('/log', function() use ($app){
	$control = new ControlerLogin($app);
    $control->connect();
})->name("login");

$app->get('/user/:id', function ($id) use ($app){
    $control = new ControleurAffichage($app);
    $control->afficherUser($id);
})->name("user");

$app->get('/get', function () use($app){
    $control = new ControleurAffichage($app);
    $control->listeProfil();
})->name("user");

$app->post('/connection', function() use ($app){
	$control = new ControlerLogin($app);
    $control->connection();
})->name("connection");

$app->get('/listelog', function() use ($app){
    $control = new ControleurAffichage($app);
    $control->afficherListeLogement();
})->name("liste");

$app->get('/listedetail/:id', function($id) use ($app){
    $control = new ControleurAffichage($app);
    $control->afficherDetailLogement($id);
})->name("listed");

$app->get('/gr/:id', function($id) use ($app){
    $control = new ControleurGroupe($app);
    $control->afficherGroupe($id);
})->name("groupe");

$app->get('/inscription', function() use ($app){
    $control = new ControleurAffichage($app);
    $control->afficherInscription();
})->name("insc");

$app->get('/connexion', function() use ($app){
    $control = new ControleurAffichage($app);
    $control->afficherConnexion();
})->name("connex");

$app->post('/register', function() use ($app){
    $control = new ControleurAuthentication($app);
    $control->inscription();
    $app->redirect('../');
})->name("reg");

$app->post('/signin', function() use ($app){
    $control = new ControleurAuthentication($app);
    $control->connexion();
	$app->redirect('../');
})->name("sign");

$app->get('/deconnexion', function() use ($app){
    $control = new ControleurAuthentication($app);
    $control->deconnexion();
	$app->redirect('../');
})->name("deconnexion");


$app->run();